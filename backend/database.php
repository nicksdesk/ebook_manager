<?php 

	require_once 'connection.php';
	require_once 'encryption.php';

	/*
		this will include endpoint functions for the api
	*/

	class Database {

		protected $connection;
		private $response;

		public function __construct(Connection $connection) {
			$this->connection = $connection;
		}

		public function getResponse() {
			return $this->response;
		}

		/*
			BEGIN USER RELATED FUNCTIONS
		*/

		//function to add an account
		public function createAccount($credentials = array()) {
			if(!empty($credentials)) {
				$email = $credentials['email'];
				$username = $credentials['username'];
				$rawPassword = $credentials['password'];
				$enc = new EncryptionHelper($rawPassword);
				$enc->encrypt();
				$password = $enc->getResponse()['hashed'];
				if($this->connection->exe("INSERT INTO users (id, username, password, email) VALUES ('', ?, ?, ?)", [$username, $password, $email])->fetch()) {
					$this->response = array('success'=>'user_created');
				} else {
					$this->response = array('error'=>'database_insertion_failed');
				}
			} else {
				$this->response = array('error'=>'empty_credentials_array');
			}
		}

		//function to authenticate a user
		public function authUser($credentials = array()) {
			if(!empty($credentials)) {
				$email = $credentials['email'];
				$username = $credentials['username'];
				$password = $credentials['password'];
				$enc = new EncryptionHelper($password);
				$result = $this->connection->exe("SELECT * FROM users WHERE username=? AND password=?", [$username, $password])->fetch();
				$enc->verify($result['password']);
				if(!empty($enc->getResponse()['success'])) {
					$this->response = array('success'=>'authenticated');
				} else if(!empty($enc->getResponse()['error'])) {	
					$this->response = array('error'=>'authentication_error');
				} else {
					$this->response = array('error'=>'unknown_error');
				}
			} else {
				$this->response = array('error'=>'empty_credentials_array');
			}
		}

		//function to reset a password
		public function resetPassword($credentials = array()) {
			if(!empty($credentials)) {
				$resetHash = genRandomChars(12);
				$userToReset = $credentials['username'];
				$userEmail = $credentials['email'];
				$result = $this->connection->exe("SELECT * FROM users WHERE username=? AND email=?", [$username, $email])->fetch();	
				if($result['username'] == $username && $result['email'] == $email) {
					/*
						TODO: gen password reset link and email it
					*/
					$rawLink = 'https://ebook.nicksdesk.com/user.update?reset=[resethash]&user=[username]';
					$link = str_replace("[resethash]", $resetHash, $rawLink);
					$link = str_replace("[username]", $username, $rawLink);
					echo $link;
				} else {
					$this->response = array('error'=>'credentials_mistmatch');
				}
			} else {
				$this->response = array('error'=>'empty_credentials_array');
			}
		}

		/*
			END USER RELATED FUNCTIONS
		*/

		/*
			BEGIN EBOOK RELATED FUNCTIONS
		*/

		//function to add a book
		public function addBook($info = array()) {
			if(!empty($info)) {
				$title = $info['book_title'];
				$author = $info['book_author'];
			} else {
				$this->response = array('error'=>'empty_info_array');
			}
		}

		//function to remove a book
		public function removeBook($info = array()) {

		}

		//modify info about a book
		public function modifyBook($info = array(), $to_change = array()) {

		}

		//connect a user to a book
		public function checkoutBook($details = array()) {

		}

		//release a book from a user
		public function returnBook($details = array()) {

		}

		/*
			END EBOOK RELATED FUNCTIONS
		*/

		/*
			BEGIN PRIVATE FUNCTIONS
		*/

		//verify json format
		private function verifyJSON($json) {
			json_decode($json);
 			return (json_last_error() == JSON_ERROR_NONE);
		}

		//gens random characters with specified length
		private function genRandomChars($len = 10) {
			$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    		$characterLength = strlen($characters);
    		$randString = '';
    		for ($i = 0; $i < $len; $i++) {
        		$randString .= $characters[rand(0, $characterLength - 1)];
    		}
    		return $randString;
		}

		/*
			END PRIVATE FUNCTIONS
		*/
	}

?>