<?php 
	
	require_once 'database.php';

	class EncryptionHelper {

		protected $password;
		private $response;

		public function __construct($password) {
			$this->password = $password;
		}

		public function encrypt($type = 0) {
			switch($type) {
				case 0:
					$hash = password_hash($this->password, PASSWORD_DEFAULT);
					$this->response = array('hashed'=>base64_encode($this->password));
				break;
				case 1:
					//md5
				break;
				case 2:
					//sha1
				break;
				default:
					//sha256
				break;
			}
		}

		public function verify($compare) {
			if($this->password != "" && $compare != "") {
				if(password_verify($this->password, base64_decode($compare))) {
					$this->response = array('success'=>'verified');
				} else {
					$this->response = array('error'=>'password_mismatch');
				}
			} else {
				$this->response = array('error'=>'empty_password');
			}
		}

		public function getResponse() {
			return $this->response;
		}

	}

?>