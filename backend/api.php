<?php 
	session_start();

	require_once 'database.php';

	$connection = new Connection("mysql:host=secure185.inmotionhosting.com;dbname=nicksd6_ebook_manager;charset=utf8", "nicksd6_ng", "ENTER_PASSWORD");
	$database = new Database($connection);

	if(isset($_POST)) {
		if(isset($_GET)) {
			parseGet($_GET);
		}
		parsePost($_POST);
	}

	function parsePost($post) {
		if(!empty($post)) {	
			$username = $post['username'];
			$password = $post['password'];
			$email = $post['email'];
			switch($post['type']) {
				case 'login':
					self::api_login($username, $password, $email);	
				break;
				case 'register':
					self::api_register($username, $password, $email);
				break;
				case 'reset':
					self::api_password_reset($username, $email);
				break;
				default:
					//add default method if no type is present
				break;
			}
		} else {
			return json_encode(array('error'=>'post_data_empty'));
		}
	}

	function parseGet($get) {
		if(!empty($get)) {
			switch($get['user']) {
				case 'reset':
					self::api_password_reset($get['username'], $get['email']);
				break;
				case 'edit':
					//add user management
				break;
				default:
					//add default case
				break;
			}
		} else {	
			return json_encode(array('error'=>'get_data_empty'));
		}
	}

	function api_register($username, $password, $email) {
		$database->register(array(
			'username'=>$username,
			'email'=>$email,
			'password'=>$password,
		));
		return json_encode($database->getResponse());
	}

	function api_login($username, $password, $email) {
		$database->login(array(
			'username'=>$username,
			'email'=>$email,
			'password'=>$password,
		));
		return json_encode($database->getResponse());
	}

	function api_password_reset($username, $email) {
		$database->resetPassword(array(
			'username'=>$username,
			'email'=>$email,
		));
		return json_encode($database->getResponse());
	}

	function api_book_manage($manage = array()) {
		if(!empty($manage)) {
			switch($manage['type']) {
				case 'add':
					$database->addBook(array(
						'book_title'=>$manage['book_title'],
						'book_author'=>$manage['book_author'],
						'book_id'=>$manage['book_id'],
						'book_tags'=>$manage['book_tags'],
						'book_year'=>$manage['book_year'],
						'book_user_rating'=>'_',
						'book_checked_out_history'=>'_',
						'book_current_readers'=>'_',
					));
				break;
				case 'remove':
					$database->removeBook(array(
						
					));
				break;
				case 'modify':
					$database->modifyBook(array(
						
					));
				break;
				case 'checkout':
					$database->checkoutBook(array(

					));
				break;
				case 'return':
					$database->returnBook(array(

					));
				break;
				default:
					//add default case
				break;
			}
		} else {
			return json_encode(array('error'=>'empty_manage_array'));
		}
	}
	
?>
