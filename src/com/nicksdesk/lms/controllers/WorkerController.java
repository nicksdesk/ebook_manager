package com.nicksdesk.lms.controllers;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.nicksdesk.helpers.Console;

public class WorkerController {

	private Object response;
	
	public WorkerController() {
		/*
		 * add optional constructor input
		 */
	}
	
	/*
	 * Runs code on a separate background thread ~ automatically allocates resources
	 */
	public void createBackgroundWorker(Runnable code) {
		try {
			Runnable bg = new Runnable() {
				public void run() {
					code.run();
				}
			};
			ExecutorService exec = Executors.newCachedThreadPool();
			exec.submit(bg);
		} catch(Exception e) {
			this.response = e.getMessage();
			Console.err(e.getMessage());
		}
	}
	
	/*
	 * Runs code on a separate background thread (repeating) ~ automatically allocates resources
	 */
	public void createRepeatingBackgroundWorker(Runnable code, int repeat) {
		try {
			Runnable bg = new Runnable() {
				public void run() {
					for(int i = 0; i < repeat; i++) {
						code.run();
					}
				}
			};
			ExecutorService exec = Executors.newCachedThreadPool();
			exec.submit(bg);
		} catch(Exception e) {
			this.response = e.getMessage();
			Console.err(e.getMessage());
		}
	}
	
	/*
	 * Runs code on a separate background thread with a timeout ~ automatically allocates resources
	 */
	public void createTaskBackgroundWorker(Runnable code, int time) {
		try {
			Timer timer = new Timer();
			Runnable bg = new Runnable() {
				public void run() {
					timer.schedule(new TimerTask() {
						public void run() {
							code.run();
						}
					}, time);
				}
			};
			ExecutorService exec = Executors.newCachedThreadPool();
			exec.submit(bg);
		} catch(Exception e) {
			this.response = e.getMessage();
			Console.err(e.getMessage());
		}
	}
	
	/*
	 * Runs code on lambda thread
	 */
	public void createControlledThread(Runnable code) {
		Runnable process = () -> {
			code.run();
		};
		Thread controlled = new Thread(process);
		controlled.start();
	}
	
	/*
	 * Response object
	 */
	public Object getResponse() {
		return response;
	}
	
}
