package com.nicksdesk.lms.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class EncryptionController {

	protected byte[] hashed = null;
	
	/*
	 * Constructor receives an integer and string
	 */
	public EncryptionController(int type, String toHash) {
		switch(type) {
			case 0:
				hashed = this.hash("SHA-256", toHash);
				break;
			case 1:
				break;
			case 2:
				break;
			default:
				break;
		}
	}
	
	/*
	 * private function that returns a byte array of the input string and hash type
	 */
	private byte[] hash(String hashType, String input) {
		try {
			MessageDigest digest = MessageDigest.getInstance(hashType);
			byte[] encoded = digest.digest(input.getBytes(StandardCharsets.UTF_8));
			return encoded;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * unhashes a hashed byte array
	 */
	public String unhash(byte[] input) {
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < input.length; i++) {
			String hex = Integer.toHexString(0xff & input[i]);
			if(hex.length() == 1) buffer.append('0');
			buffer.append(hex);
		}
		return buffer.toString();
	}
	
	/*
	 * returns current hashed byte array
	 */
	public byte[] getHashed() {
		return hashed;
	}
	
}
