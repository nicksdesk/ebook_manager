package com.nicksdesk.lms.controllers;

import java.util.HashMap;

import com.nicksdesk.helpers.Console;

public class UserController {

	/*
	 * general response object variable
	 */
	private Object response;
	private EncryptionController encController;
	
	/*
	 * constructor setups up registration and login
	 */
	public UserController(HashMap<String, HashMap<String, String>> account, String type) {
		if(account.containsKey("user")) {
			String email = account.get("user").get("email");
			String username = account.get("user").get("username");
			String password = account.get("user").get("password");
			if(!(username.equalsIgnoreCase("")) && !(password.equalsIgnoreCase("")) && !(type.equalsIgnoreCase("")) && !(email.equalsIgnoreCase(""))) {
				String[] posts = {
					"type:"+type+";",
					"email:"+email+";",
					"username:"+username+";",
					"password:"+password+";",
				};
				WorkerController wc = new WorkerController();
				wc.createBackgroundWorker(new Runnable() {
					public void run() {
						ConnectionController connection = new ConnectionController(posts, 0.1);
						if(connection.buildConnection()) Console.log(connection.getResponse());
						Console.err("Error receiving a valid connection response!");
					}
				});				
				String wcResponse = ((String) wc.getResponse());
				if(!wcResponse.isEmpty()) {
					Console.log(wcResponse);
				} else {
					this.response = "empty_thread_response";
				}
			} else {
				this.response = "empty_account_array";
			}
		} else {
			this.response = "user_key_empty";
		}
	}
	

	public Object getResponse() {
		return response;
	}
	
}
