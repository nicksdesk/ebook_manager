package com.nicksdesk.lms.controllers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.nicksdesk.helpers.Console;

public class ConnectionController {

	/*
	 * TODO: setup api endpoint
	 */
	private double API_VERSION = 0.1;
	private boolean useSSL = false;
	private String API = "https://ebook.nicksdesk.com/api.php";
	
	/*
	 * Main response variable... all functions are void except for getters so responses go here
	 * (includes data that will be encoded and sent to backend)
	 */
	private HashMap<Object, Object> response = new HashMap<Object, Object>();
	private String data = "";
	
	/*
	 * Connection variable definitions
	 */
	private URL connection;
	private HttpURLConnection finalConnection;
	
	/*
	 * Final variables (should never have to change)
	 */
	private final String METHOD = "POST";
	private final String TYPE = "application/x-www-form-urlencoded";
	private final String AGENT = "Mozilla/5.0";
	
	private final HashMap<String, String> mappedFields = new HashMap<String, String>();
	
	/*
	 * Constructor sets up field variables to send to api
	 */
	public ConnectionController(String[] fields, double version) {
		this.API_VERSION = (version > 0) ? version : 0.1;
		for(int i = 0; i < fields.length; i++) {
			String[] points = fields[i].split(";");
			for(int f = 0; f < points.length; f++) {
				mappedFields.put(points[f].split(":")[0], points[f].split(":")[1]);
			}
		}
	}
	
	/*
	 * Sets up connection to api
	 */
	public boolean buildConnection() {
		StringBuilder content = new StringBuilder();
		String line;
		String vars = "";
		String vals = "";
		if(!(this.getRawFields().equalsIgnoreCase(""))) {
			if(this.useSSL) this.API.replaceAll("http", "https");
			try {
				for(Map.Entry<String, String> entry : mappedFields.entrySet()) {
					vars = entry.getKey();
					vals = entry.getValue();
					this.data += ("&"+vars+"="+vals);
				}
				if(data.startsWith("&")) {
					data.replaceFirst("&", "");
				}
				connection = new URL(this.API);
				BufferedReader reader = new BufferedReader(new InputStreamReader(this.getConnection(this.connection, this.data)));
				while((line = reader.readLine()) != null) {
					content.append(line + "\n");
				}
				reader.close();
				this.response.put("response", content.toString());
				return true;
			} catch(Exception e) {
				Console.err(e.getMessage());
				return false;
			}
		} else {
			this.response.put("response", "raw_fields_empty");
			return false;
		}
	}
	
	/*
	 * Finalize the connection (only accessed by this class)
	 */
	private InputStream getConnection(URL url, String data) {
		try {
			byte[] out = data.toString().getBytes();
			finalConnection = (HttpURLConnection) url.openConnection();
			finalConnection.setRequestMethod(METHOD);
			finalConnection.setDoOutput(true);
			finalConnection.addRequestProperty("User-Agent", AGENT);
			finalConnection.addRequestProperty("Content-Type", TYPE);
			finalConnection.connect();
			try {
				OutputStream os = finalConnection.getOutputStream();
				os.write(out);
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
			return finalConnection.getInputStream();
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Converts mappedFields HashMap to a string
	 */
	public String getRawFields() {
		return this.mappedFields.toString();
	}
	
	/*
	 * Returns general response variable as object
	 */
	public Object getResponse() {
		return this.response;
	}
	
	/*
	 * Returns current set api version
	 */
	public double getApiVersion() {
		return this.API_VERSION;
	}
	
	/*
	 * sets the endpoint url
	 */
	public void setApi(String api) {
		this.API = (this.API.isEmpty()) ? api : this.API;
	}
	
}
