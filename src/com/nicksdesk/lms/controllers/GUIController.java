package com.nicksdesk.lms.controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

import com.nicksdesk.helpers.Console;
import com.nicksdesk.helpers.OSHelper;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUIController extends Application {

	private String path;
	private boolean loadFromConnection = true;
	private String fxmlFromApi = null;
	
	public GUIController(String path) {
		this.path = path;
	}
	
	public GUIController() {
		/*
		 * don't need to use local path
		 */
	}
	
	public GUIController loadGUIFromConnection(String url) {
		StringBuilder guiCache = new StringBuilder();
		try {
			URL connect = new URL(url);
			BufferedReader reader = new BufferedReader(new InputStreamReader(connect.openStream()));
			String line;
			while((line = reader.readLine()) != null) {
				guiCache.append(line + "\n");
			}			
			OSHelper os = new OSHelper(3);
			String homePath = System.getProperty("user.home");
			switch(os.getOS()) {
				case 0:
					//windows fs
					File winCacheDirs = new File(homePath + File.separator + "Documents" + File.separator + "ebook_cache");
					if(winCacheDirs.mkdirs()) {
						File winCacheFile = new File(winCacheDirs.getAbsolutePath() + File.separator + "gui.cache");
						BufferedWriter writer = new BufferedWriter(new FileWriter(winCacheFile));
						writer.write(guiCache.toString());
						writer.close();
						this.fxmlFromApi = winCacheFile.getAbsolutePath();
						return this;
					} else {
						return this;
					}
				case 1:
					//macos fs
					File macCacheDirs = new File(homePath + "/Documents/" + "ebook_cache/");
					if(macCacheDirs.mkdirs()) {
						
					} else {
						return this;	
					}
				case 2:
					//linux fs
					return this;
				default:
					/*
					 * no os detected, can't write cache
					 */
					return this;
			}
		} catch(Exception e) {
			Console.err(e.getMessage());
			return this;
		}
	}

	
	/*
	 * (non-Javadoc)
	 * TODO: fix fxml loading
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root;
		if(!this.path.isEmpty() && this.path != null) {
			if(!this.loadFromConnection) { 
				root = FXMLLoader.load(GUIController.class.getResource(path)); 
			} else {
				root = FXMLLoader.load(new File(this.fxmlFromApi).toURI().toURL());
			}
			primaryStage.setTitle("EBook Manager");
			primaryStage.setScene(new Scene(root, 800, 500));
			primaryStage.show();
		}
	}
	
	
	public void begin(String[] args) {
		launch(args);
	}
                                                    
	
}
