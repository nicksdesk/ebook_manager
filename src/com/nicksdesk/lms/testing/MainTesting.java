package com.nicksdesk.lms.testing;

import com.nicksdesk.lms.controllers.GUIController;

public class MainTesting {

	public static void main(String[] args) {
		GUIController guiController = new GUIController();
		guiController.loadGUIFromConnection("https://nicksdesk.com/login_form.fxml");
		guiController.begin(args);
	}
	
}
