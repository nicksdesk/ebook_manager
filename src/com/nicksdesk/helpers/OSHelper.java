package com.nicksdesk.helpers;

public class OSHelper {

	//3 is try automatic os detection: 0=window,1=macos,2=linux_dist,3=unknown/not set
	private int os = 3;

	public OSHelper(int os) {
		this.os = os;
	}

	//returns: 0,1,2 [0=windows,1=macos,2=dist of linux,-1=unknown]
	public int validateOS() {
		switch(this.os) {
			case 0:
				this.optimize(0);
				return 0;
			case 1:
				this.optimize(1);
				return 1;
			case 2:
				this.optimize(2);
				return 2;
			case 3:
				String osName = System.getProperty("os.name");
				if(osName.contains("Win")) {
					this.optimize(0);
					return 0;
				} else if(osName.contains("Mac")) {
					this.optimize(1);
					return 1;
				} else if(osName.contains("Lin")) {
					this.optimize(2);
					return 2;
				} else {
					return -1;
				}
			default:
				this.os = 3;
				validateOS();
				break;
		}
		return 3;
	}
	
	public int getOS() {
		return this.validateOS();
	}
	
	private void optimize(int type) {
		/*
		 * TODO: add optimization code
		 */
	}
	
}
