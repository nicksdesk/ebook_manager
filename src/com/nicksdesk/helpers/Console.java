package com.nicksdesk.helpers;

public class Console {
	
	public static void log(Object obj) {
		System.out.println(obj);
	}
	
	public static void err(Object obj) {
		System.err.println(obj);
	}
	
}
