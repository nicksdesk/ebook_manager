package application;
	
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

import com.nicksdesk.helpers.Console;
import com.nicksdesk.helpers.OSHelper;
import com.nicksdesk.lms.controllers.GUIController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
	
	private String path = "";
	private boolean loadFromConnection = true;
	private static String fxmlFromApi = null;
	
	public static void main(String[] args) {
		loadGUIFromConnection("https://nicksdesk.com/login_form.fxml");
		launch(args);
	}
	
	private static void loadGUIFromConnection(String url) {
		String homePath = System.getProperty("user.home");
		StringBuilder guiCache = new StringBuilder();
		try {
			URL connect = new URL(url);
			/*
			 * TODO: add a check to see if cache exists and is updated, if so run re-download in background so launch doesn't take as long
			 */
			boolean cacheExists = new File(homePath + File.separator + "Documents" + File.separator + "ebook_cache").exists();
			if(!cacheExists) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connect.openStream()));
				String line;
				while((line = reader.readLine()) != null) {
					guiCache.append(line + "\n");
				}	
			}			
			OSHelper os = new OSHelper(3);
			switch(os.getOS()) {
				case 0:
					//windows fs
					File winCacheDirs = new File(homePath + File.separator + "Documents" + File.separator + "ebook_cache");
					if(winCacheDirs.mkdirs()) {
						File winCacheFile = new File(winCacheDirs.getAbsolutePath() + File.separator + "gui.cache");
						BufferedWriter writer = new BufferedWriter(new FileWriter(winCacheFile));
						writer.write(guiCache.toString());
						writer.close();
						fxmlFromApi = winCacheFile.getAbsolutePath();
						Console.log(fxmlFromApi);
					}
					break;
				case 1:
					//macos fs
					File macCacheDirs = new File(homePath + "/Documents/" + "ebook_cache/");
					if(macCacheDirs.mkdirs()) {
						
					} else {
						
					}
					break;
				case 2:
					//linux fs
					break;
				default:
					/*
					 * no os detected, can't write cache
					 */
					break;
			}
		} catch(Exception e) {
			Console.err(e.getMessage());
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root;
		if(!this.path.isEmpty() && this.path != null) {
			if(!this.loadFromConnection) { 
				root = FXMLLoader.load(GUIController.class.getResource(path)); 
			} else {
				Console.log(fxmlFromApi);
				root = FXMLLoader.load(new File(fxmlFromApi).toURI().toURL());
			}
			primaryStage.setTitle("EBook Manager");
			primaryStage.setScene(new Scene(root, 800, 500));
			primaryStage.show();
		}
	}
	
}
